from variables import *

osbuild = {
    "^BaseOS$": [
      {
        "name": "rhel-guest-image",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["qcow2"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64", "ppc64le", "aarch64", "s390x"],
        "repo": ["BaseOS","AppStream",]
      },
      {
        "name": "rhel-ec2",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64"],
        "repo": ["BaseOS","AppStream",
                 "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "ec2",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client",
                "redhat-cloud-client-configuration",
            ],
        },
      },
      {
        "name": "rhel-ec2-aarch64",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["aarch64"],
        "repo": ["BaseOS","AppStream",
                 "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "ec2",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client",
                "redhat-cloud-client-configuration",
            ],
        },
      },
      {
        "name": "rhel-ec2-3p",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64", "aarch64"],
        "repo": ["BaseOS","AppStream",
                 "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "ec2-3p",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client",
                "redhat-cloud-client-configuration-cdn",
            ],
        },
      },
      {
        "name": "rhel-azure",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["azure-rhui"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64", "aarch64"],
        "repo": [
          "BaseOS", "AppStream",
          "https://download.devel.redhat.com/devel/stratosphere-rhui-sync/rhui-microsoft-azure-rhel9/"
        ],
        "subvariant": "vhd",
        "customizations": {
            "packages": [
              "rhui-azure-rhel9",
              "redhat-cloud-client-configuration-cdn",
            ],
            "rpm": {
              "import_keys": {
                "files": [
                  "/etc/pki/rpm-gpg/RPM-GPG-KEY-microsoft-azure-release",
                ],
              },
            },
        },
      },
    ],
    "^HighAvailability$": [
      {
        "name": "rhel-ha-ec2",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2-ha"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64"],
        "repo": ["BaseOS","AppStream", "HighAvailability",
                 "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "HighAvailability",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client-ha",
                "redhat-cloud-client-configuration",
            ],
        },
      },
    ],
    "^SAPHANA$": [
      {
        "name": "rhel-sap-ec2",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2-sap"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64"],
        "repo": ["BaseOS","AppStream", "HighAvailability", "SAP", "SAPHANA",
                  "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "SAPHANA",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client-sap-bundle-e4s",
                "redhat-cloud-client-configuration",
            ],
        },
      },
      {
        "name": "rhel-sap-ec2-3p",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["ec2-sap"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64"],
        "repo": ["BaseOS","AppStream", "HighAvailability", "SAP", "SAPHANA",
                  "https://download.devel.redhat.com/rhel-9/rel-eng/RHUI/latest-RHUI-Client-4-RHEL-9/compose/RHUI/$arch/os/"
        ],
        "subvariant": "SAPHANA-3p",
        "customizations": {
            "packages": [
                "rh-amazon-rhui-client-sap-bundle-e4s",
                "redhat-cloud-client-configuration-cdn",
            ],
        },
      },
      {
        "name": "rhel-sap-ha-azure",
        "version": "%s" % RELEASE_VERSION_XY,
        "distro": "rhel-%s" % RELEASE_VERSION_XY,
        "image_types": ["azure-sap-rhui"],
        "target": "guest-rhel-%s-image" % RELEASE_VERSION_XYZ,
        "arches": ["x86_64"],
        "repo": ["BaseOS", "AppStream", "HighAvailability", "SAP", "SAPHANA",
                  "https://download.devel.redhat.com/devel/stratosphere-rhui-sync/rhui-microsoft-azure-rhel9-sap-ha",
        ],
        "subvariant": "SAPHANA",
        "customizations": {
            "packages": [
              "rhui-azure-rhel9-sap-ha",
              "redhat-cloud-client-configuration-cdn",
            ],
            "rpm": {
              "import_keys": {
                "files": [
                  "/etc/pki/rpm-gpg/RPM-GPG-KEY-microsoft-azure-release",
                ],
              },
            },
        },
      },
    ],
}
